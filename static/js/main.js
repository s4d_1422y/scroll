var READY = true;
function scrollOnFlag(block, trigger, direction){
    if (trigger && READY){
        if (direction){ // down
            READY = false;
            block.removeClass("current-block");
            block.next().addClass("current-block");
            READY = true;
        } else {
            READY = false;
            block.removeClass("current-block");
            block.prev().addClass("current-block");
            READY = true;
        }
    }
}

function formMenu(){
    $(".data").each(function(index){
        $(".menuitself").append("<div onclick=\"goto("+index+")\">" + index + ". " + $(this).attr("title") + "</div>");
        $(this).attr("index", index);
    });
    $(".menuitself").addClass("visible");
}

function goto(reqindex){
    $(".data").each(function(index){
       $(this).removeClass("current-block");
        if ($(this).attr("index") == reqindex) $(this).addClass("current-block");
    });
    $(".menuitself").removeClass("visible");
    $(".menuitself").empty();
}

$(document).ready((event) => {
    $(window).bind('mousewheel', function(event) {
        if ((event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) && READY) {
            //up
            if($(".current-block").hasClass("head")) return;
            scrollOnFlag($(".current-block"), true, false);
        }
        else if (!(event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) && READY) {
            //down
            if($(".current-block").hasClass("tail")) return;
            scrollOnFlag($(".current-block"), true, true);
        }
    })
});
